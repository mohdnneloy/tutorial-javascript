// To activate the code just remove the comment operators '/*' and '*/' from the section you want to run ============= 

// Section 1 Variables

/*
let statement = 'Checking second script';
const number = 20;
let float = 20.01756;
console.log(statement);
console.log(number);
console.log(float.toPrecision(5));
*/

// Section 2 DataTypes

/*
let name = 'Neloy'; // String Literal
let number = 30; // Number Literal
let flag = true; // Boolean Type
let firstName = undefined; // At declaration of variable it is set to undefined, Undefined Type
let color = null; // Value with Null Value, Object Type

console.log(typeof name);
*/

// Section 3 Objects

/*
let person = {

    name: 'Neloy',
    age: 21

};

console.log(person);
console.log(person.name); // Dot Notation
console.log(person['age']); // Bracket Notation
*/

// Section 4 Arrays

/*
let colorsWnumber = ['red', 'blue', 20];
console.log(colorsWnumber);
console.log(colorsWnumber[1]);
console.log(colorsWnumber.length);
*/

// Section 5 Functions 

/*
function hello1(){
    console.log('Hello World');
}

function hello2 (name){
    console.log('Hello '+ name);
}

function hello3 (fname, lname){
    console.log('Hello '+ fname + ' ' + lname);
}

function nameR (fname, lname){
    return fname + ' ' + lname; 
}


//We need to call the fucntions otherwise it will not work

hello1();
hello2('Neloy');
hello3('Robert', 'Blues');
console.log(nameR('Dominic', 'Toretto'));
*/

/*==================== THE NETNINJA EXTRA BASIC JAVASCRIPT==========================*/

// Section 10 document.write and console.lod

/*
var number = 20;
document.write(number); // Write onto the whole document
console.log(number); // Updates the console on the browser
*/

/*
var number = 20;
document.write(number); // Write till here first
document.writ(30); // Add this line later on the console after you finish till the last line before this line
*/

// Section 12 Booleans in JS

/*
var istrue = true;
var istrue = false;
console.log(istrue);
*/

// Section 14 Additional comparison operators

/*
var x = "5"; // x here is a string
x==5 // string compared with integer // As value is only compared it will return true
x === 5 // string compared with integer // As type is compared it will return false
x!=5 // returns false as value matches
x!==5 // returns true as type does not match
*/

// Section 17 For loops in JS

/*
for(n = 0; n<10; n++){
    cosole.log(n);
 }
 */

 // Section 20 Practical Example 

 /*
 var links = document.getElementsByTagName("a");
 
 for(i=0; i<links.length; i++){
    links[i].className = "hello-class-"+i
}
 */

// Section 22 Numbers

/*
var a = "7";
var b = 7;
var c = 5;
console.log(a+c);
console.log(b+c);
*/

/*
var a = "7";
var b = 7.;
var c = 5;
console.log(typeof(a+c)); // String
console.log(typeof(b+c)); // Number or integer value
*/

/*
Math.round(7.5); // Rounds off the number that is 8 here
Math.floor(7.9); // Gets the floor value that is 7 here
Math.ceil(7.3); // Gets the ceil value that is 8 here
Math.max(7,4,9,8); // Finds the max value out of the given values
Math.min(7,4,9,8); // Finds the min value out of the given values
Math.PI // Prints the pi value
*/

// Section 23 Not a Number "NaN"

/*
var a = "apple";
var b = 5;
console.log(a*b); // Result "NaN"
console.log(isNaN(a*b));
*/

// Section 24 Strings

/*
var string = "Hello I'm the Net Ninja";
var string2 = "Hello I\"m the Net Ninja"; // Try without the backslash
console.log(string2);
*/

/*
var string = "Hello World";

console.log(string.length); // Gives the length of the string
console.log(string.toLowerCase()); // Turns the string to lower to lower case
console.log(string.toUpperCase()); // Turns the string to lower to upper case
console.log(string.indexOf("World")); // Gives the first index of the given word within the string
                                       // If not found it returns -1
*/

// 25 Slice nad Split Strings

/*
var string = "Hello World";
var string2 = string.slice(0,5);
console.log(string2);
*/

/*
var string = "chicken, beef, fish, tuna"; // String 
var meatarray = string.split(",") // Form array element upto the given character for every term in the string
console.log(meatarray);
*/

// Section 26 Arrays

/*
var arr1 = []; // Declared, the insert any element by specifying index, starting with index 0;
arr1[0] = "Anything";
var arr2 = [20,1.1, "hello", false]; // Unlike C++ it can have any value with an array // Direct declaration and initialization
var arr3 = new Array(); // Similar as the first declaration process
var arr4 = new Array(5); // Array size can be specified in this case // It does not really matter
console.log(arr1);
console.log(arr2);
console.log(arr3);
console.log(arr4);
*/

/*
var arr = [20,1.1, "hello", false];
console.log(arr.length + " Elements in the array");
console.log(arr.sort()); // This will sort the array in incresing order
console.log(arr.reverse()); // This will reverse the given array 
*/

// 28 Creating New Objects 

/*
var mycar = new Object();
mycar.company = "Mercedes";
mycar.model = "W12";

mycar.move = function (){
    console.log("The car is moving");
 } // For calling use mycar.move()

 
console.log("Car Company: " + mycar.company);
console.log("Car Model: " + mycar.model);
mycar.move();
*/

/*
var mycar2 = {company: "Mercedes",
               model: "W12",
               move: function(){
                        console.log("The car is moving");
                     }, 
                speed: function(distance, time){
                        console.log("Speed: "+ (distance/time) + " Kmh");
                    }   
                };

console.log("Car Company: " + mycar2.company);
console.log("Car Model: " + mycar2.model);
mycar2.move();
mycar2.speed(20,10);
*/

/*
var mycar = { company: "Mercedes",
              model: "W12",
              printDetails: function(){
                     console.log("Car Company: " + this.model);
                     console.log("Car Model: " + this.model);
             },
               printObject: function(){
                  console.log(this);
               } 
            };

mycar.printDetails();
mycar.printObject();
*/


// Section 30 Constructor Functions

/*
var Car = function(){

    this.speed = 20;
    console.log("Speed: " + this.speed);
}

var car1 = new Car();
console.log(car1.speed);
*/

/*
var Car = function(speed, model){

    this.speed = speed;
    this.model = model;
    console.log("Speed: " + this.speed);
    console.log("Model: " + this.model);
    
    this.move = function(speed){
            console.log("Car is moving a speed of " + speed);
    };

};

var car1 = new Car(10, "W12");
console.log(car1.speed);
car1.move(30);

var car2 = car1; // Acts as copy constructor
console.log(car2.model);
*/

// Section 31 The Date Object 

/*
var newdate = new Date(); // Current Date details
console.log(newdate); 

var newdate2 = new Date(1998, 11, 05, 22, 11, 00); // Specified Date details
console.log(newdate2);
*/

/*
var bdday = new Date(1998, 10, 05, 22, 11, 00);
var bdday2 = new Date(1998, 10, 05, 22, 11, 00);

console.log(bdday.getMonth()); // Gives the month
console.log(bdday.getFullYear()); // Gives the year
console.log(bdday.getDate()); // Gives the date day number
console.log(bdday.getDay()); // Gives the number for the day, Number ranges from '0-6' where '0' is Sunday and '6' is Saturday
console.log(bdday.getHours()); // Gives the hours in a '0-23' format
console.log(bdday.getTime()); // Number of milliseconds from '1 January 1970' to the date entered or the current day in empty object

if(bdday == bdday2) // Checking Objects
   console.log("Birthdays are equal");
else 
   console.log("Birthdays are not equal");

if(bdday.getTime() == bdday2.getTime()) // Checking milliseconds
   console.log("Birthdays are equal");
else 
   console.log("Birthdays are not equal");
   */

// Section 
